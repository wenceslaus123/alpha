package com.sourceit.alpha.adapter;

import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sourceit.alpha.R;
import com.sourceit.alpha.model.Currency;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Currency> list;
    private Context context;
    OnListClick onListClick;
    ViewHolder viewHolder;


    private int showExchangeFlag;

    public static final int SHOW_EXCHANGE = 1;
    public static final int NOT_SHOW_EXCHANGE = 2;


    public interface OnListClick {
        void onItemClick(ViewHolder viewHolder, int position);
    }


    public void setOnListClick(OnListClick onListClick) {
        this.onListClick = onListClick;
    }


    public Currency getItem(int position) {
        return list.get(position);
    }


    public ListAdapter(Context context, List<Currency> list, int showExchangeFlag, OnListClick onListClick) {
        this.list = list;
        this.context = context;
        this.showExchangeFlag = showExchangeFlag;
        this.onListClick = onListClick;
    }

    @Override
    public int getItemViewType(int position) {

        return showExchangeFlag;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;

        switch (viewType) {
            case NOT_SHOW_EXCHANGE:
                View v = LayoutInflater.from(context).inflate(R.layout.item_layout, parent, false);
                viewHolder = new ViewHolder(v);
                break;
            case SHOW_EXCHANGE:
                View v2 = LayoutInflater.from(context).inflate(R.layout.item_search_layout, parent, false);
                viewHolder = new ViewHolderExchange(v2);
                break;
        }
        return viewHolder;

    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        switch (holder.getItemViewType()) {
            case NOT_SHOW_EXCHANGE:
                ((ViewHolder) holder).updateData(list.get(position));
                ViewCompat.setTransitionName(((ViewHolder) holder).currencyItem, "currency_item_" + String.valueOf(position));
                ((ViewHolder) holder).currencyItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onListClick.onItemClick((ViewHolder) holder, position);
                    }
                });
                break;
            case SHOW_EXCHANGE:
                ((ViewHolderExchange) holder).updateDataExchange(list.get(position));
                break;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return list.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image_currency)
        ImageView imageViewCurrency;
        @BindView(R.id.name_currency)
        TextView nameCurrency;
        public @BindView(R.id.currency_item)
        LinearLayout currencyItem;

        Currency data;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void updateData(Currency data) {
            this.data = data;
            imageViewCurrency.setImageResource(data.image);
            nameCurrency.setText(String.format("%s\n%s", data.base, data.fullName));

        }
    }

    class ViewHolderExchange extends RecyclerView.ViewHolder {

        @BindView(R.id.image_currency)
        ImageView imageViewCurrency;

        @BindView(R.id.name_currency)
        TextView nameCurrency;

        @BindView(R.id.ex_rate_currency)
        TextView exRateCurrency;

        Currency data;

        public ViewHolderExchange(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void updateDataExchange(Currency data) {
            this.data = data;
            imageViewCurrency.setImageResource(data.image);
            nameCurrency.setText(data.base);


            //FIXME
            //Map<String, Float> rates;
            exRateCurrency.setText("100");

        }

        @OnClick(R.id.root)
        void onItemClick() {
            if (onListClick != null) {
                onListClick.onItemClick(viewHolder, getAdapterPosition());
            }

        }
    }

}
