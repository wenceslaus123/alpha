package com.sourceit.alpha.fragment;


import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;

import com.sourceit.alpha.R;
import com.sourceit.alpha.adapter.DescriptionAdapter;
import com.sourceit.alpha.model.Currency;
import com.sourceit.alpha.util.Retrofit;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/*
Расширенная версия приложения должна содержать весь функционал базовой версии и дополнительный экран поиска.
 На этот экран пользователь попадает по долгому клику на одну из валют в списке на первом или втором экранах.
 Он должен содержать иконку, код и название валюты, строку поиска курса по дате (текстовое поле и кнопку поиска),
 а также список валют с курсом обмена, согласно указанной дате.

Приоритет задач.
создать минимально жизнеспособное приложение согласно требованиям выше.
установить текущую дату, как дату для поиска по умолчанию
добавить обработку ошибочного ввода даты.

добавить обработку ситуации отсутствия соединения с интернетом.
добавить навигацию по клику на элементы списка (переход на экраны 2 и 3).


 */
public class SearchFragment extends Fragment implements DatePickerDialog.OnDateSetListener {
    @BindView(R.id.search_list)
    RecyclerView listView;

    @BindView(R.id.edit_text_search)
    EditText editTextSearch;

    DescriptionAdapter searchAdapter;

    Calendar currentDate;
    DatePickerDialog datePickerDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_search, container, false);
        ButterKnife.bind(this, view);

        currentDate = Calendar.getInstance();

        int year = currentDate.get(Calendar.YEAR);
        int month = currentDate.get(Calendar.MONTH) ;
        int day = currentDate.get(Calendar.DAY_OF_MONTH);

        editTextSearch.setText(day + "-" + (month +1)+ "-" + year);
        datePickerDialog = new DatePickerDialog(getContext(), this, year, month, day);

        editTextSearch.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                datePickerDialog.show();
                return false;
            }
        });
        buttonSearchClick();
        return view;
    }


    @OnClick(R.id.button_search)
    public void buttonSearchClick() {

        String strDateToFind = convertString(editTextSearch.getText().toString().toLowerCase());

        Retrofit.getByDate(strDateToFind, new Callback<Currency>() {
            @Override
            public void success(Currency data, Response response) {
                LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                listView.setLayoutManager(layoutManager);
                searchAdapter = new DescriptionAdapter(data.rates, getContext());
                listView.setAdapter(searchAdapter);
            }
            @Override
            public void failure(RetrofitError error) {
                editTextSearch.setText(error.toString());
            }
        });
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        editTextSearch.setText(String.valueOf(dayOfMonth) + "-" + String.valueOf(month+1) +
                "-" + String.valueOf(year));
    }

    public String convertString(String strDateToFind)
    {
        //20/02/2018
        String[] tmpStr = strDateToFind.split("-");
        if (tmpStr[1].length()==1) tmpStr[1]="0"+tmpStr[1];
        if (tmpStr[0].length()==1) tmpStr[0]="0"+tmpStr[0];

        //{"base":"EUR","date":"2000-01-23","rates":
        return   tmpStr[2]+"-"+tmpStr[1]+"-"+tmpStr[0];
    }

}