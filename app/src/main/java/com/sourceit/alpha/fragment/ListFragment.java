package com.sourceit.alpha.fragment;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Fade;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.sourceit.alpha.adapter.ListAdapter;
import com.sourceit.alpha.R;
import com.sourceit.alpha.model.Currency;
import com.sourceit.alpha.util.CurrencyGenerator;
import com.sourceit.alpha.util.DescriptionTransition;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


import static com.sourceit.alpha.adapter.ListAdapter.NOT_SHOW_EXCHANGE;


public class ListFragment extends Fragment implements ListAdapter.OnListClick{

    ListAdapter listAdapter;

    @BindView(R.id.list)
    RecyclerView listView;

    List<Currency> dataList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        ButterKnife.bind(this, view);


        dataList = CurrencyGenerator.list();

        listAdapter = new ListAdapter(getContext(), dataList, NOT_SHOW_EXCHANGE, this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        listView.setLayoutManager(layoutManager);
        listView.setAdapter(listAdapter);

        return view;
    }

    @Override
    public void onItemClick(ListAdapter.ViewHolder viewHolder, int position) {
        Currency currency = listAdapter.getItem(position);
        DescriptionFragment descriptionFragment = DescriptionFragment.newInstance(currency);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            descriptionFragment.setSharedElementEnterTransition(new DescriptionTransition());
            descriptionFragment.setEnterTransition(new Fade());
            setExitTransition(new Fade());
            descriptionFragment.setSharedElementReturnTransition(new DescriptionTransition());
        }

        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .addSharedElement(viewHolder.currencyItem, "currency_item")
                .replace(R.id.activity_main, descriptionFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.calendar:
                SearchFragment searchFragment = new SearchFragment();
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.activity_main, searchFragment)
                        .addToBackStack(null)
                        .commit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}