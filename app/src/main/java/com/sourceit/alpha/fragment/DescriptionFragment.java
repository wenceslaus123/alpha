package com.sourceit.alpha.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sourceit.alpha.R;
import com.sourceit.alpha.adapter.DescriptionAdapter;
import com.sourceit.alpha.model.Currency;
import com.sourceit.alpha.util.Retrofit;


import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class DescriptionFragment extends Fragment {

    Currency currency;

    @BindView(R.id.image_currency)
    ImageView imageCurrencyDescription;
    @BindView(R.id.name_currency)
    TextView currencyName;
    @BindView(R.id.currency_description)
    TextView currencyDescription;
    @BindView(R.id.list_description)
    RecyclerView listViewDescription;


    public static DescriptionFragment newInstance(Currency currency) {
        Bundle args = new Bundle();
        args.putParcelable("currency", currency);
        DescriptionFragment fragment = new DescriptionFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            currency = getArguments().getParcelable("currency");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.fragment_description, container, false);
        ButterKnife.bind(this, view);
        currencyName.setText(String.format("%s\n%s", currency.base, currency.fullName));
        currencyDescription.setText(currency.description);
        imageCurrencyDescription.setImageResource(currency.image);

        Retrofit.getCurrency(currency.base, new Callback<Currency>() {
            @Override
            public void success(Currency currencyApi, Response response) {
                currency.date = currencyApi.date;
                currency.rates = currencyApi.rates;

                LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                listViewDescription.setLayoutManager(layoutManager);
                DescriptionAdapter descriptionAdapter = new DescriptionAdapter(currency.rates, getContext());
                listViewDescription.setAdapter(descriptionAdapter);
            }

            @Override
            public void failure(RetrofitError error) {
                currencyDescription.setText(error.toString());
            }
        });

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.calendar:
                SearchFragment searchFragment = new SearchFragment();
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.activity_main, searchFragment)
                        .addToBackStack(null)
                        .commit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
